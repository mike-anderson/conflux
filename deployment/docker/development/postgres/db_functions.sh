#!/bin/bash
#
# MAINTAINER Echosec Platform Team <dev@echosec.net>

#
# All these commands disable pseudo-tty allocation for Jenkins (-T flag)
#
# I have no ideas as to why this is necessary for Jenkins ¯\_(ツ)_/¯
#
# We source the Laravel environment files to prevent duplicate
# definitions of the same variable.
#

wait_for_db(){
  source ../../../.env
  set -x
  docker-compose exec -T $DB_HOST ./wait-for-it.sh -h $DB_HOST -p $DB_PORT
}

create_user(){
  source ../../../.env
  # http://stackoverflow.com/questions/8092086/create-postgresql-role-user-if-it-doesnt-exist
  SQL="
  DO
  \$body$
  BEGIN
     IF NOT EXISTS (
        SELECT *
        FROM   pg_catalog.pg_user
        WHERE  usename = '${DB_USERNAME}') THEN

        CREATE ROLE ${DB_USERNAME} LOGIN PASSWORD '${DB_PASSWORD}';
     END IF;
  END
  \$body$;
  "
  echo -e ">> Creating User: ${DB_USERNAME}"
  docker-compose exec -T $DB_HOST psql -U postgres -c "${SQL}"
}

# Main DB Functionality
#######################

drop_main_db(){
  source ../../../.env
  terminate_active_sessions
  echo -e ">> Drop database: ${DB_DATABASE}"
  docker-compose exec -T $DB_HOST dropdb $DB_DATABASE --user postgres --if-exists
  echo -e ">> Dropped database: ${DB_DATABASE}"
}

create_main_db(){
  source ../../../.env
  echo -e ">> Create database: ${DB_DATABASE}"
  docker-compose exec -T $DB_HOST su postgres -c "createdb -O ${DB_USERNAME} '${DB_DATABASE}'"
  echo -e ">> Created database: ${DB_DATABASE}"
}

# Test DB Functionality
#######################

drop_test_db(){
  source ../../../.env.testing
  terminate_active_sessions
  echo -e ">> Drop database: ${DB_DATABASE}"
  docker-compose exec -T $DB_HOST dropdb $DB_DATABASE --user postgres --if-exists
  echo -e ">> Dropped database: ${DB_DATABASE}"
}

create_test_db(){
  source ../../../.env.testing
  echo -e ">> Create database: ${DB_DATABASE}"
  docker-compose exec -T $DB_HOST su postgres -c "createdb -O ${DB_USERNAME} '${DB_DATABASE}'"
  echo -e ">> Created database: ${DB_DATABASE}"
}

terminate_active_sessions() {
  echo -e ">> Terminating all active sessions"
  SQL="BEGIN;
    SELECT pg_terminate_backend(pg_stat_activity.pid)
    FROM pg_stat_activity
    WHERE pg_stat_activity.datname = '${DB_DATABASE}' AND pid <> pg_backend_pid();
  END;"
  docker-compose exec -T $DB_HOST psql -U postgres -c "${SQL}"
}

# Call arguments verbatim from the command line
$@
