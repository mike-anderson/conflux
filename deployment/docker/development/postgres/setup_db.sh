#!/bin/bash
#
# MAINTAINER Echosec Platform Team <dev@echosec.net>

set -e   # If there is a error abort immediately

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) # Get the directory of this file

source $SCRIPT_DIR/db_functions.sh

main(){
  drop_main_db
  create_user
  create_main_db
}

main
