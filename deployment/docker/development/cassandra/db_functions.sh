
wait_for_db() {

  CASSANDRA_WAIT_TIMEOUT=${CASSANDRA_WAIT_TIMEOUT:-"60"}

  total_wait=0
  while true
  do
    docker-compose exec -T ${CASSANDRA_HOST:-"cassandra"} cqlsh -e "describe keyspaces"
    if (( $? == 0 )); then
      break
    else
      if (( total_wait >= ${CASSANDRA_WAIT_TIMEOUT} )); then
        echo "Timed out waiting for Cassandra."
        exit 1
      fi
      echo "Cassandra is still not up. Waiting 1 second."
      sleep 1s
      ((total_wait++))
    fi
  done

}

create_keyspace_from_template() {
  trace_ttl=${TRACE_TTL:-172800}
  dependencies_ttl=${DEPENDENCIES_TTL:-0}

  template=$1
  if [[ "$template" == "" ]]; then
      template=${SCRIPT_DIR}/${SCHEMA}.${VERSION}.cql.tmpl
  fi

  if [[ "$MODE" == "" ]]; then
      echo "missing MODE parameter"
  elif [[ "$MODE" == "prod" ]]; then
      if [[ "$DATACENTER" == "" ]]; then usage "missing DATACENTER parameter for prod mode"; fi
      datacenter=$DATACENTER
      replication_factor=${REPLICATION_FACTOR:-2}
      replication="{'class': 'NetworkTopologyStrategy', '$datacenter': '${replication_factor}' }"
  elif [[ "$MODE" == "test" ]]; then 
      datacenter=${DATACENTER:-'test'}
      replication_factor=${REPLICATION_FACTOR:-1}
      replication="{'class': 'SimpleStrategy', 'replication_factor': '${replication_factor}'}"
  else
      echo "invalid MODE=$MODE, expecting 'prod' or 'test'"
  fi

  keyspace=${KEYSPACE:-"${SCHEMA}_${VERSION}_${datacenter}"}

  if [[ $keyspace =~ [^a-zA-Z0-9_] ]]; then
      usage "invalid characters in KEYSPACE=$keyspace parameter, please use letters, digits or underscores"
  fi

  echo "creating keyspace from template with:
      template: ${template}
      keyspace: ${keyspace}
      replication: $replication"

  # strip out comments, collapse multiple adjacent empty lines (cat -s), substitute variables
  cat $template | sed \
      -e 's/--.*$//g'                                 \
      -e 's/^\s*$//g'                                 \
      -e "s/\${keyspace}/${keyspace}/g"               \
      -e "s/\${replication}/${replication}/g"         \
      -e "s/\${trace_ttl}/${trace_ttl}/g"             \
      -e "s/\${dependencies_ttl}/${dependencies_ttl}/g" \
      | cat -s \
      | docker exec -i $(docker-compose ps -q ${CASSANDRA_HOST:-"cassandra"}) cqlsh
}

drop_keyspace() {
  keyspace=${1:-"${SCHEMA}_${VERSION}_${MODE}"}
  echo "drop keyspace: ${keyspace}"
  docker-compose exec -T ${CASSANDRA_HOST:-"cassandra"} cqlsh -e "DROP KEYSPACE IF EXISTS ${keyspace};"
}




