#!/bin/bash

set -e   # If there is a error abort immediately

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) # Get the directory of this file

source $SCRIPT_DIR/db_functions.sh

main() {
  MODE=test
  SCHEMA=jaeger
  VERSION=v1
  TRACE_TTL=0

  wait_for_db
  drop_keyspace
  create_keyspace_from_template
}

main