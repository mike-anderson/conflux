COMPOSE_DIR := ./deployment/docker/development
SRC_DIR := ./src
BASE_DIR := ${CURDIR}

box: up db

logs:
	cd ${COMPOSE_DIR} && docker-compose logs

# Bring the development environment up
up:
	cd ${COMPOSE_DIR} && docker-compose up -d && docker-compose ps

# Tear the development environment down
down:
	cd ${COMPOSE_DIR} && docker-compose down

# Create the conflux database
db: postgres cassandra

postgres:
	cd ${COMPOSE_DIR} && ./postgres/setup_db.sh

cassandra:
	cd ${COMPOSE_DIR} && ./cassandra/setup_db.sh