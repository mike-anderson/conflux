import { Module, Global } from '@nestjs/common';
import { jaegerCassandraProvider } from  './jaeger.provider';
import { confluxCassandraProvider } from  './conflux.provider';

@Global()
@Module({
  providers: [jaegerCassandraProvider, confluxCassandraProvider],
  exports: [jaegerCassandraProvider, confluxCassandraProvider],
})
export class DatabaseModule {}