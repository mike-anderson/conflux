import * as cassandra from 'cassandra-driver';

export const confluxCassandraProvider = {
  
  provide: 'confluxCassandraProvider',

  useFactory: async () => {
    const client = new cassandra.Client({ 
      contactPoints: ['localhost'], 
      keyspace: 'conflux_v1_test' 
    });
    await client.connect();
    return client;
  }
}