import * as cassandra from 'cassandra-driver';

export const jaegerCassandraProvider = {
  
  provide: 'jaegerCassandraProvider',

  useFactory: async () => {
    const client = new cassandra.Client({ 
      contactPoints: ['localhost'], 
      keyspace: 'jaeger_v1_test' 
    });
    await client.connect();
    return client;
  }
}