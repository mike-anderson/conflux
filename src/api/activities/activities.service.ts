import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Activity } from './activity.entity';
import { CreateActivityDTO } from './activities.dto';

@Injectable()
export class ActivitiesService {

  constructor (@InjectRepository(Activity) 
    private readonly activityRepository:Repository<Activity>) {}

  async create (activity: CreateActivityDTO) {
    const record = this.activityRepository.create(activity);
    console.log(record);
    await this.activityRepository.save(record);
    return record;
  }

  async findAll () {
    return this.activityRepository.find();
  }

  async find (id: string) {
    return this.activityRepository.findOneOrFail(id);
  }

}