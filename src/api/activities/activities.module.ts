import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ActivitiesController } from './activities.controller';
import { ActivitiesService} from './activities.service';
import { Activity } from './activity.entity';
import { RunsController } from './runs/runs.controller';
import { RunsService } from './runs/runs.service';
import { Run } from './runs/run.entity';
import { TracesController } from './runs/traces/traces.controller';
import { TracesService } from './runs/traces/traces.service';
import { TracesModule} from 'api/traces/traces.module';
 
@Module({
  imports: [TracesModule, TypeOrmModule.forFeature([Activity, Run])],
  providers: [ActivitiesService, RunsService, TracesService],
  controllers: [ActivitiesController, RunsController, TracesController]
})
export class ActivitiesModule {}
 