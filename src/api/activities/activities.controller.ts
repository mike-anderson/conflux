import { Controller, Body, Get, Post, Param, Res, BadRequestException } from '@nestjs/common';
import { CreateActivityDTO } from './activities.dto';
import { ActivitiesService } from './activities.service';


@Controller('activities')
export class ActivitiesController {

  constructor(private readonly ActivitiesService: ActivitiesService) {}

  @Post()
  async create(@Body() activity: CreateActivityDTO) {
    const record = await this.ActivitiesService.create(activity);
    return {
      activities: [ record ], 
    };
  }

  @Get()
  async findAll() {
    const records = await this.ActivitiesService.findAll();
    return {
      activities: records, 
    };
  } 

  @Get(':id')
  async find(@Param('id') id) {
    try{
      const record = await this.ActivitiesService.find(id);
          return {
      activities: [ record ], 
    };
    } catch (e) {
      throw new BadRequestException('the requested activity does not exist');
    }
  }

}
