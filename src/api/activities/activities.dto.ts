
export class ActivityDTO {
  readonly id:          number;
  readonly name:        string;
  readonly description: string;
}

export class CreateActivityDTO {
  readonly name:        string;
  readonly description: string;
}

export class UpdateActivityDTO {
  
}
