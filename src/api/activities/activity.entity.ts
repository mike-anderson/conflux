import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Run } from './runs/run.entity';

@Entity('activities')
export class Activity {
  @PrimaryGeneratedColumn() 
  id: number;

  @Column()
  name: string;

  @Column('text') 
  description: string;

  @OneToMany(type => Run, run => run.activity)
  runs: Promise<Run[]>;
}