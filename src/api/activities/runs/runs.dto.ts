
export class RunDTO {
  readonly id:          number;
  readonly description: string | null;
  readonly activity_id: number;
  readonly start_time:  number;
  readonly end_time:    number | null; 
  readonly is_running:  boolean;
}

export class CreateRunDTO {
  readonly activity_id: number | undefined;
  readonly description: string | undefined;  
  readonly start_time:  number | undefined;
  readonly end_time:    number | undefined; 
  readonly is_running:  boolean | undefined;
}

export class UpdateRunDTO {
  readonly id:          number;
  readonly description: string | undefined;
  readonly start_time:  number | undefined;
  readonly end_time:    number | undefined;
  readonly is_running:  boolean | undefined;
}