import { Injectable, BadRequestException } from '@nestjs/common';
import { TracesRepository } from '../../../traces/traces.repository';
import { RunsService } from '../runs.service';


@Injectable()
export class TracesService {

  constructor (
    private readonly runsService: RunsService,
    private readonly tracesRepository:TracesRepository
  ) {}

  async findAll(run_id: number) {
    const runRecord = await this.runsService.find(run_id);
    const start_time = runRecord.start_time;
    const end_time = runRecord.end_time;

    if (!end_time) {
      throw new BadRequestException('the run must be complete before traces can be viewed, stop the run');
    }

    return await this.tracesRepository.findTraceIdsBetween(start_time, end_time);

  }

}
