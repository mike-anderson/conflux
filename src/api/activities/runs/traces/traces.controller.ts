import { Controller, Get, Param } from '@nestjs/common';
import { TracesService } from './traces.service';
import { ActivitiesService } from '../../activities.service';
import { RunsService } from '../runs.service';
import { RunsController } from '../runs.controller';
import { TracesRepository } from 'api/traces/traces.repository';

@Controller('activities/:activity_id/runs/:run_id/traces')
export class TracesController {

  constructor(
    private readonly activitiesService: ActivitiesService,
    private readonly runsService: RunsService,
    private readonly tracesService:TracesService,
    private readonly tracesRepository: TracesRepository
  ) {}

  @Get()
  async findAll(
    @Param('activity_id') activity_id,
    @Param('run_id')run_id, 
  ) {
    const activityRecord = await this.activitiesService.find(activity_id);
    const runRecord = await this.runsService.find(run_id);
    const traceIds = await this.tracesService.findAll(run_id);
    return {
      activities: [ activityRecord ],
      runs: [ RunsController.toDTO(runRecord) ],
      trace_ids: traceIds,
    }
  }

  @Get('/summary') 
  async summary(    
    @Param('activity_id') activity_id,
    @Param('run_id')run_id, 
  ){
    const traceIds = await this.tracesService.findAll(run_id);
    let rootSpans = await Promise.all(traceIds.map( async (id) => {
      let rootSpan = await this.tracesRepository.getRootSpan(id);
      let spanCount = await this.tracesRepository.getSpanCount(id);
      let events = await this.tracesRepository.getQueueEvents(id);
      let tables = await this.tracesRepository.getDBTables(id);
      let timings = await this.tracesRepository.getTimingsTotals(id);
      let api_calls = await this.tracesRepository.getApiCalls(id);
      return {
        id: id,
        operation_name: rootSpan.operation_name,
        start_time: rootSpan.start_time,
        duration: rootSpan.duration,
        span_count: spanCount,
        api_calls: api_calls,
        events: events,
        tables: tables,
        timings: timings,
      };
    }));

    rootSpans.sort( (a,b) => a.start_time - b.start_time)

    const events = rootSpans.reduce( (a,c) => {
      return a.concat(Object.keys(c.events).map(k => c.events[k]));
    }, []);
    const tables = rootSpans.reduce( (a,c) => {
      Object.keys(c.tables).forEach( k => {
        if (!a[k]) {
          a[k] = Object.assign({},c.tables[k]);
        } else {
          a[k].count += c.tables[k].count;
          a[k].tables = [...a[k].tables, ...c.tables[k].tables];
        }
      })
      return a;
    },{});
    const summary = {
      traces: rootSpans.length,
      spans: rootSpans.reduce((a,c) => a + c.span_count, 0),
      async_events: events.length,
      db_reads: tables.select.count,
      db_writes: tables.insert.count + tables.update.count + tables.delete.count,
      external_api_calls: rootSpans.reduce((a,c) => a + c.api_calls.length, 0),
      non_blocked_process_time: rootSpans.reduce((a,c) => a + c.timings.self_duration, 0),
      total_process_time: rootSpans.reduce((a,c) => a + c.timings.blocked_duration + c.timings.self_duration, 0),
      duration: rootSpans.map( n => n.timings.end_time).reduce( (a,c) => a == null || c > a ? c : a, null) - 
                rootSpans.map( n => n.timings.start_time).reduce( (a,c) => a == null || c < a ? c : a, null)
    };

    return {
      summary: summary,
      traces: rootSpans,
      events: events,
      tables: tables,
    };

    // const spanCount = await this.traces.getSpanCount(id);
    // const rootSpan = await this.traces.getRootSpan(id);
    // const events = await this.traces.getQueueEvents(id);
    // const tables = await this.traces.getDBTables(id)
    // const externalCalls = await this.traces.getApiCalls(id);

    // return {
    //   trace: {
    //     operation_name: rootSpan.operation_name,
    //     start_time: rootSpan.start_time,
    //     duration: rootSpan.duration,
    //     span_count: spanCount,
    //   },
    //   queue_events: events,
    //   database: tables
    // }

  }
}
