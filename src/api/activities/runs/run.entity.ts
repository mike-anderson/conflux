import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, AfterLoad, BeforeInsert, BeforeUpdate, JoinColumn, AfterUpdate } from 'typeorm';
import { Activity } from '../activity.entity';

@Entity('runs')
export class Run {
  @PrimaryGeneratedColumn() id: number;

  @Column({ nullable: true })
  description: string;

  @Column('timestamp') 
  start_time: Date;

  @Column({ type: 'timestamp',
    nullable: true })
  end_time: Date;

  @Column({nullable:true})
  activity_id: number;

  @ManyToOne(type => Activity, activity => activity.runs)
  @JoinColumn({name: 'activity_id'})
  activity: Activity;

  is_running: boolean;

  @AfterLoad()
  @AfterUpdate()
  checkIfRunning() {
      this.is_running = this.end_time ? false : true;
  }

  @BeforeInsert()
  @BeforeUpdate()
  validateTimes() {
    if (this.start_time && this.end_time && this.start_time >= this.end_time) {
      throw 'the start_time must be less than the end_time';
    }  
  }
}