import { Controller, Body, Get, Post, Param, Put, Res, BadGatewayException, Delete } from '@nestjs/common';
import { CreateRunDTO, UpdateRunDTO, RunDTO } from './runs.dto';
import { RunsService } from './runs.service';
import { ActivitiesService } from '../activities.service';
import { Run } from './run.entity';

@Controller('activities/:activity_id/runs')
export class RunsController {

  constructor(
    private readonly activitiesService: ActivitiesService,
    private readonly runsService: RunsService
  ) {}

  public static toDTO (run: Run) {
    let record = {
      ...run
    } as any;
    if (record.start_time) {
      record.start_time = run.start_time.valueOf() / 1000 | 0;
    }
    if (record.end_time) {
      record.end_time = run.end_time.valueOf() / 1000 | 0;
    }
    return <RunDTO> record;
  }

  public static fromDTO (record: RunDTO) {
    let run = {
      ...record
    } as any;
    if (run.start_time) {
      run.start_time = new Date(record.start_time*1000);
    }
    if (run.end_time) {
      run.end_time = new Date(record.end_time*1000);
    }
    return <Run> run;
  }

  @Post()
  async create(
    @Param('activity_id') activity_id: number, 
    @Body() run: CreateRunDTO
  ) {
    const activityRecord = await this.activitiesService.find(activity_id);
    const runRecord = await this.runsService.create(
      RunsController.fromDTO(<RunDTO> {
        activity_id: activity_id,
        ...run
      })
    );
    return {
      activities: [ activityRecord ],
      runs:  [ RunsController.toDTO(runRecord) ],
    }
  }

  @Put(':id')
  async update(
    @Param('activity_id') activity_id: number,
    @Param('id') id: number, 
    @Body() run: UpdateRunDTO  
  ) {
    const activityRecord = await this.activitiesService.find(activity_id);
    const runRecord = await this.runsService.update(
      RunsController.fromDTO({
        ...run,
        id: id,
        activity_id: activity_id
      })
    );
    return {
      activities: [ activityRecord ],
      runs:  [ RunsController.toDTO(runRecord) ],
    }
  }

  @Get()
  async findAll(
    @Param('activity_id') activity_id
  ) {
    const activityRecord = await this.activitiesService.find(activity_id);
    const runs = await this.runsService.findAllForActivity(activity_id);
    const runrecords = runs.map(RunsController.toDTO);
    return {
      activities: [ activityRecord ],
      runs: runrecords, 
    };
  } 

  @Get(':id')
  async find(
    @Param('activity_id') activity_id,
    @Param('id') id
  ) {
    const activityRecord = await this.activitiesService.find(activity_id);
    const record = await this.runsService.find(id);
    return {
      activities: [ activityRecord ],
      runs: [ RunsController.toDTO(record) ], 
    };
  }

  @Delete(':id')
  async delete (
    @Param('activity_id') activity_id,
    @Param('id') id
  ) {
    await this.runsService.delete(id);
  }

}

