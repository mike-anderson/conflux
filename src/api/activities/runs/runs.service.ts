import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { Run } from './run.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RunsService {

  constructor (
    @InjectRepository(Run) private readonly RunRepository:Repository<Run>
  ) {}

  async create (run: Run) {
    const activity_id = run.activity_id;
    const start_time = run.start_time ? run.start_time : new Date();
    const end_time = run.end_time ? run.end_time : null;

    const alreadyRunning = await this.findRunningForActivity(run.activity_id);
    if (alreadyRunning) {
      throw new BadRequestException(`run ${alreadyRunning.id} is already running for this activity, stop or delete it to create another`);
    }

    return this.RunRepository.save({
      ...run,
      activity_id,
      start_time,
      end_time,
    });
  }

  async update(run: Run) {
    const record = await this.RunRepository.findOneOrFail(run.id);
    if (run.is_running == false && !run.end_time && !record.end_time) {
      run.end_time = new Date();
    }
    delete run.id;
    this.RunRepository.merge(record, run);

    return this.RunRepository.save(record);
  }

  async delete(id: number) {
    this.RunRepository.delete(id);
  }

  async find (id: number) {
    return this.RunRepository.findOneOrFail(id);
  }

  async findAll () {
    return this.RunRepository.find();
  }

  async findAllForActivity (activity_id: number) {
    return this.RunRepository.find({activity_id});
  }

  async findRunningForActivity (activity_id: number) {
    return this.RunRepository.findOne({activity_id: activity_id, end_time: null});
  }
  

}
