import { Injectable, Inject } from '@nestjs/common';
import { Client } from 'cassandra-driver';
const btoa = require('btoa');

function keyValueAsObject (keyValue) {
  return keyValue.reduce( (acc, item) => {
    acc[item.key] = item[`value_${item.value_type}`];
    return acc;
  },{});
}

@Injectable()
export class TracesRepository {

  constructor (@Inject('jaegerCassandraProvider') private readonly client:Client) {}

  async spanQuery(id: string) {
    const query = 'SELECT * FROM traces WHERE trace_id=?';
    return this.client.execute(query, [ Buffer.from(id, 'hex') ], { prepare: true });
  }

  async findTraceIdsBetween(start_time: Date, end_time: Date) {
    const query = `SELECT trace_id 
                   FROM traces 
                   WHERE start_time > ? 
                      AND start_time < ?
                   ALLOW FILTERING;`;
    const params = [
      start_time.valueOf() * 1000, (end_time.valueOf() + 1000) * 1000
    ];
    const results = await this.client.execute(query, params, { prepare: true });
    const traceIds = results.rows.map( row => row.trace_id.toString('hex') );
    return [...new Set(traceIds)];
  }

  async getSpans(id: string) {
    const result = await this.spanQuery(id);
    return result.rows;
  }

  async getRootSpan(id: string) {
    const result = await this.spanQuery(id);
    return result.rows.filter( row => row.refs === null).find( r => true );
  }

  async getSpanCount(id: string) {
    const result = await this.spanQuery(id);
    return result.rowLength;
  }

  async getActions(id: string) {
    const result = await this.spanQuery(id);
    const actions = {};
    result.rows.forEach((row) => {
      if (!actions[row.operation_name]) {
        actions[row.operation_name] = 0;
      }
      actions[row.operation_name] += 1;
    });
    return actions;
  }

  async getQueueEvents(id: string) {
    const result = await this.spanQuery(id);
    const queueEvents = {};

    result.rows.forEach((row) => {

      if (row.tags) {
        const tags = keyValueAsObject(row.tags);
        if (tags.queue_id) {
          queueEvents[tags.queue_id] = {
            ...queueEvents[tags.queue_id],
            operation_name: row.operation_name,
            start_time: row.start_time,
            duration: row.duration,
            service: row.process.service_name
          };
        }
      }

      if (row.logs) {
        row.logs.forEach((log) => {
          const fields = keyValueAsObject(log.fields);
          if (fields.id && fields.type) {
            queueEvents[fields.id] = {
              ...queueEvents[fields.id],
              parent_operation_name: row.operation_name,
              dispatch_time: log.ts
            };
          }
        })
      }
    });

    return queueEvents;
  }

  async getDBCalls(id: string) {
    const result = await this.spanQuery(id);
    const parentSpans = {};

    result.rows.forEach((row) => {
      if (row.operation_name == 'PDOStatement::execute') {
        const tags = keyValueAsObject(row.tags);
        const parent = row.refs[0].span_id;
        if (!parentSpans[parent]) {
          parentSpans[parent] = {
            db_events: []
          };
        }
        parentSpans[parent].db_events.push(tags.query);
      }
    });

    result.rows.forEach((row) => {
      if (parentSpans[row.span_id]) {
        parentSpans[row.span_id].operation_name = row.operation_name;
      }
    });

    return parentSpans;
  }

  async getDBTables(id: string) {
    const result = await this.spanQuery(id);
    const dbQueries = [];
    const dbOperations = {
      select:{count:0,tables:new Set()},
      update:{count:0,tables:new Set()},
      insert:{count:0,tables:new Set()},
      delete:{count:0,tables:new Set()},
    }

    result.rows.forEach( (row) => {
      if (row.operation_name === 'PDOStatement::execute') {
        let tags = keyValueAsObject(row.tags);
        dbQueries.push(tags.query);
      }
    });

    dbQueries.forEach( query => {
      const tokens = query.toLowerCase().split(" ");
      const operation = tokens[0];
      let tables = tokens.reduce( (acc, v, i) => {
        if (v === 'from' || v === 'join' || v === 'into' || v === 'update') {
          return [...acc, tokens[i+1].replace(/"/g,'')];
        }
        return acc;
      }, []);
      if (dbOperations[operation]) {
        dbOperations[operation].count += 1;
        tables.forEach (table => {
          dbOperations[operation].tables.add(table);
        })
      }
    });

    Object.values(dbOperations).forEach( op => {
      op.tables = [...op.tables]; 
    });

    return dbOperations;
  }

  async getApiCalls(id: string) {
    const result = await this.spanQuery(id);
    return result.rows.reduce( (calls, node) => {
      if (node.operation_name === "GuzzleHttp\\Client::request" || 
          node.operation_name === "GuzzleHttp\\Client::send") {
        let tags = keyValueAsObject(node.tags)
        let operation_name = tags.method + ":" + tags.uri;
        calls.push(operation_name);
      }
      return calls;
    },[]);
  }

  async getTaskTree(id: string) {
    const result = await this.spanQuery(id);
    const nodes = {};
    let rootNode;

    // convert spans to nodes
    result.rows.forEach( row => {
      let node = {
        id: row.span_id,
        trace_id: row.trace_id.toString('hex'),
        parent_id: row.refs ? (row.refs[0] ? row.refs[0].span_id : null) : null,
        service_name: row.process.service_name,
        operation_name: row.operation_name,
        start_time: Number(row.start_time),
        duration: Number(row.duration),
        children: [],
        async_children: [],
        tags: row.tags ? keyValueAsObject(row.tags) : {},
        logs: row.logs ? row.logs.map( log => {
          return {
            ts: Number(log.ts),
            ... keyValueAsObject(log.fields),
          };
        }) : [],
      }

      // convert spans to nodes using some custom rules
      if (node.operation_name === 'PDOStatement::execute') {
        node.service_name = 'postgres';
        node.operation_name = node.tags.query;
      } if (node.operation_name === 'PDO::__construct') {
        node.service_name = 'postgres';
        node.operation_name = 'connect';
      }
      if (node.operation_name === "GuzzleHttp\\Client::request" || 
          node.operation_name === "GuzzleHttp\\Client::send") {
        node.service_name = 'external-' + node.tags.uri;
        node.operation_name = node.tags.method;
      }
      if (node.operation_name === 'Pusher::trigger') {
        node.service_name = 'external-pusher';
        node.operation_name = 'trigger';
      }
      if (node.operation_name.includes('Cache')) {
        node.service_name = 'redis';
        node.operation_name = node.operation_name.split('::')[1];
      }
      node['process_id'] = node.tags.process_id ? node.tags.process_id : null;

      // more custom rules for converting log events to "follows-from"
      if (node.tags.queue_id) {
        node['async_id'] = node.tags.queue_id;
      }
      nodes[node.id] = node;
      if (!node.parent_id) {
        rootNode = node;
      }
    });

    // populate children
    Object.values(nodes).forEach( node => {
      if (node.parent_id && nodes[node.parent_id]) {
        if (node.async_id) {
          nodes[node.parent_id].async_children.push(node);
					node['dispatch_time'] = Number(nodes[node.parent_id]
            .logs.find( log => log.id === node.async_id )
            .ts);
        } else {
          nodes[node.parent_id].children.push(node);
        }
      }
    });

    // recursively sort children
    function sortChildren (node) {
      function compareChildren(n1,n2) {
        return n1.start_time - n2.start_time;
      }
      node.children.forEach(sortChildren);
      node.children.sort(compareChildren);
      node.async_children.forEach(sortChildren);
      node.async_children.sort(compareChildren);
    }
    sortChildren(rootNode);

    // recursively calculate timing
    function computeTiming (node) {
      node.children.forEach(computeTiming);
      node.async_children.forEach(computeTiming);
      node.timing = {
        self_duration: node.duration,
        blocked_duration: 0,
        percent_self_duration: 1,
        events: [],
      }
      node.children.forEach( child => {
        node.timing.events.push({
          id: child.id,
          service: child.service_name,
          process: child.process_id,
          task: child.operation_name,
          type: 'blocking',
          blocking_duration: child.duration,
          relative_ts: child.start_time - node.start_time, 
        })
      });
      node.async_children.forEach( child => {
        node.timing.events.push({
          id: child.id,
          service: child.service_name,
          process: child.process_id,
          task: child.operation_name,
          type: 'async',
          blocking_duration: 0,
          relative_ts: child.dispatch_time - node.start_time, 
        })
      });
      node.timing.events.sort((e1, e2) => e1.relative_ts - e2.relative_ts);
      node.timing.events.forEach( event => {
        event.relative_ts -= node.timing.blocked_duration;
        if (event.type === 'blocking') {
          node.timing.blocked_duration += event.blocking_duration;
          node.timing.self_duration -= event.blocking_duration;
        }
      });
      node.timing.percent_self_duration = node.timing.self_duration / node.duration;
    }
    computeTiming(rootNode);

    return rootNode;
  }

  async getTimingsTotals (id: string) {
    const rootNode = await this.getTaskTree(id);
    const totals = {
      blocked_duration: 0,
      self_duration: 0,
      start_time: null,
      end_time: null,
    };
    function collectTimings(node) {
      node.children.forEach(collectTimings);
      node.async_children.forEach(collectTimings);
      if (!node.service_name.includes('external')) {
        totals.blocked_duration += node.timing.blocked_duration;
        totals.self_duration += node.timing.self_duration;
      }
      if (!totals.start_time || node.start_time < totals.start_time) {
        totals.start_time = node.start_time;
      }
      if (node.duration < 1000000000 && (!totals.end_time || node.start_time + node.duration > totals.end_time)) {
        totals.end_time = node.start_time + node.duration;
      }
    }
    collectTimings(rootNode);
    totals['duration'] = totals.end_time - totals.start_time;
    return totals;
  }

  async getTimingsSummary (id: string) {
    const rootNode = await this.getTaskTree(id);
    const timings = {};
    function collectTimings(node) {
      node.children.forEach(collectTimings);
      node.async_children.forEach(collectTimings);
      if (!timings[node.service_name]) {
        timings[node.service_name] = {}
      }
      if (!timings[node.service_name][node.operation_name]) {
        timings[node.service_name][node.operation_name] = {
          statistics: {
            num_calls: 0,
            avg_self_duration: 0,
            min_self_duration: 0,
            max_self_duration: 0,
            avg_blocked_duration: 0,
            min_blocked_duration: 0,
            max_blocked_duration: 0,
          },
          calls: [],
        }
      }
      const task = timings[node.service_name][node.operation_name];
      task.statistics.num_calls += 1;
      task.calls.push({
        id: node.id,
        self_duration: node.timing.self_duration,
        blocked_duration: node.timing.blocked_duration,
      });
      task.statistics.avg_self_duration = task.calls.reduce( (a,c) => c.self_duration + a, 0) / task.statistics.num_calls;
      task.statistics.avg_blocked_duration = task.calls.reduce( (a,c) => c.blocked_duration + a, 0) / task.statistics.num_calls;
      task.statistics.min_self_duration = Math.min(...task.calls.map( c => c.self_duration ));
      task.statistics.min_blocked_duration = Math.min(...task.calls.map( c => c.blocked_duration ));
      task.statistics.max_self_duration = Math.max(...task.calls.map( c => c.self_duration ));
      task.statistics.max_blocked_duration = Math.max(...task.calls.map( c => c.blocked_duration ));
    }
    collectTimings(rootNode);

    return timings;
  }

  async getCSV(id: string) {
    const result = await this.spanQuery(id);
    let rows = [
      [
        "id",
        "parent_id",
        "process",
        "service",
        "operation_name",
        "tags",
        "start_time",
        "end_time"
      ]
    ];

    result.rows.forEach( row => {
      let id = row.span_id;
      let parent_id = row.refs ? (row.refs[0] ? row.refs[0].span_id : null) : null;
      let process_name = row.process.service_name;
      let service_name = row.process.service_name.replace(/(-[0-9]+)*/g,"");
      let operation_name = row.operation_name;
      let start_time = Number(row.start_time);
      let duration = row.duration;
      let tags = row.tags ? btoa(JSON.stringify(keyValueAsObject(row.tags))) : "";
      switch (operation_name) {
        case "GuzzleHttp\\Client::send":
          service_name = 'external-'+keyValueAsObject(row.tags).uri.split('?')[0];
          break;
        case 'PDOStatement::execute':
          service_name = 'postgres';
          break;
        case 'Pusher::trigger':
          service_name = 'pusher';
          break;
        case 'Cache::get':
          service_name = 'redis';
          break;
        case 'Cache::get':
          service_name = 'redis';
          break;
      }
      rows.push([
        id,
        parent_id,
        process_name,
        service_name,
        operation_name,
        tags,
        start_time,
        duration
      ])
    });
    //TODO: Sort this
    return rows;
  }
}
