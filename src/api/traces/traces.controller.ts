import { Controller, Get, Param, Res, forwardRef } from '@nestjs/common';
import { TracesRepository } from './traces.repository';
import { identity } from 'rxjs';

@Controller('traces')
export class TracesController {
  constructor(private readonly traces:TracesRepository) {}

  @Get(':id')
  async find(@Param('id') id) 
  {
    let spans = await this.traces.getSpans(id);
    return spans;
  }

  @Get(':id/summary') 
  async summary(@Param('id') id)
  {
    const spanCount = await this.traces.getSpanCount(id);
    const rootSpan = await this.traces.getRootSpan(id);
    const events = await this.traces.getQueueEvents(id);
    const tables = await this.traces.getDBTables(id)
    // const externalCalls = await this.traces.getApiCalls(id);

    return {
      trace: {
        operation_name: rootSpan.operation_name,
        start_time: rootSpan.start_time,
        duration: rootSpan.duration,
        span_count: spanCount,
      },
      queue_events: events,
      database: tables
    }

  }

  @Get(':id/count')
  async count(@Param('id') id) 
  {
    let count = await this.traces.getSpanCount(id);
    return {
      id: id,
      count: count
    };
  }

  @Get(':id/actions')
  async actions(@Param('id') id) 
  {
    let actions = await this.traces.getActions(id);
    return actions;
  }

  @Get(':id/events')
  async events(@Param('id') id) 
  {
    let events = await this.traces.getQueueEvents(id);
    return events;
  }

  @Get(':id/db')
  async db(@Param('id') id) 
  {
    let db = await this.traces.getDBCalls(id);
    return db;
  }

  @Get(':id/rows')
  async rows(@Param('id') id, @Res() res) 
  {
    let rows = await this.traces.getCSV(id);
    res.set('Content-Type', 'text/csv');
    rows.forEach( row => {
      res.write(row.toString() + "\n");
    });
    res.end();
  }

  @Get(':id/tree')
  async tree(@Param('id') id)
  {
    return this.traces.getTaskTree(id);
  }

  @Get(':id/tree/timings')
  async timings(@Param('id') id)
  {
    return this.traces.getTimingsSummary(id);
  }

}
