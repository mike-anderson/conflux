import { Module } from '@nestjs/common';
import { TracesController } from './traces.controller';
import { TracesRepository } from './traces.repository';

@Module({
  controllers: [TracesController],
  providers: [TracesRepository],
  exports: [TracesRepository]
})
export class TracesModule {}
