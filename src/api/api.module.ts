import { Module } from '@nestjs/common';

import { DatabaseModule } from 'database/database.module';
import { TypeOrmModule } from "@nestjs/typeorm";
import { TracesModule } from './traces/traces.module';
import { ActivitiesModule } from './activities/activities.module';

@Module({
  imports: [
      TypeOrmModule.forRoot(),
      DatabaseModule, 
      TracesModule, 
      ActivitiesModule]
})
export class ApiModule {}
