import { NestFactory } from '@nestjs/core';
import { ApiModule } from 'api/api.module';

async function bootstrap() {
  const api = await NestFactory.create(ApiModule);
  await api.listen(3005);
}
bootstrap();
